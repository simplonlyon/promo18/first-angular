import { Injectable } from '@angular/core';
import { Dog } from './entities';

@Injectable({
  providedIn: 'root'
})
export class DogProviderService {

  constructor() { }

  getDogs(): Dog[] {
    return [
      { id: 1, name: 'Doge', breed: 'Shiba', birthdate: new Date('2021-01-03') },
      { id: 2, name: 'Cheddar', breed: 'Corgi', birthdate: new Date('2019-02-13') },
      { id: 3, name: 'Muf', breed: 'Crossbred', birthdate: new Date('2022-02-10') },
      { id: 4, name: 'Pola', breed: 'Samoyed', birthdate: new Date('2020-10-06') },
      { id: 5, name: 'Jérôme', breed: 'Dog', birthdate: new Date('2021-04-09') }
    ];
  }

  /* getDogs() {
    let date = new Date(2/122)
    let fluffy : Dog = {id:0, name:"Hudson", breed:"Labrador", birthDate:date}
    let Hudson : Dog = {id:1, name:"fluffy", breed:"Fox", birthDate:date}
    let Margot : Dog = {id:2, name:"Argo", breed:"Yorkshire", birthDate:date}
    let list : Dog[] = [fluffy, Hudson, Margot];
    return list;
  } */

  getDogsOrderedById() {
    let list = this.getDogs();
    let listOrdered = list.sort((a, b) => (a.id > b.id)? 1: -1);
    return listOrdered;
  }

  getDogsOrderedByName() {
    let list = this.getDogs();
    let listOrdered = list.sort((dog1, dog2) => {
      if(dog1.name.toLowerCase() < dog2.name.toLowerCase()) {
        return -1;
      }
      if(dog1.name.toLowerCase() > dog2.name.toLowerCase()) {
        return 1;
      }
      return 0
    });
    return listOrdered;
  }

  getDogsOrderedByBirth() {
    let list = this.getDogs();
    let listOrdered = list.sort((dog1, dog2) => {
      if(dog1.birthdate.getTime() < dog2.birthdate.getTime()) {
        return -1;
      }
      if(dog1.birthdate.getTime() > dog2.birthdate.getTime()) {
        return 1;
      }
      return 0
    });
    return listOrdered;
  }

  getDogsOrderedByBreed() {
    let list = this.getDogs();
    let listOrdered = list.sort((a, b) => (a.breed.toLowerCase() > b.breed.toLowerCase())? 1: -1);
    return listOrdered;
  }


}
