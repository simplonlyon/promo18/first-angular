import { TestBed } from '@angular/core/testing';

import { DogProviderService } from './dog-provider.service';

describe('DogProviderService', () => {
  let service: DogProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DogProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
