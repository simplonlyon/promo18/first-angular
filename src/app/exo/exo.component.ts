import { Component } from '@angular/core';

@Component({
  selector: 'app-exo',
  templateUrl: './exo.component.html',
  styleUrls: ['./exo.component.css']
})
export class ExoComponent{

  counter:number = 0;

  /**
   * Méthode pour incrémenter la valeur du compteur
   */
  public increment():void {
    this.counter++;
  }

  /**
   * Méthode pour decrémenter la valeur du compteur
   */
  public decrement():void {
    this.counter--;
  }

  /**
   * Méthode pour remettre à zéro la valeur du compteur
   */
  public reset():void {
    this.counter = 0;
  }
}
