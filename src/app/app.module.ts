import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { FirstComponent } from './first.component';
import { ExoComponent } from './exo/exo.component';
import { TemplatingComponent } from './templating/templating.component';
import { StudentComponent } from './student/student.component';
import { DogListComponent } from './dog-list/dog-list.component';
import { DogComponent } from './dog/dog.component';
import { BreedManagerComponent } from './breed-manager/breed-manager.component';


@NgModule({
  declarations: [
    FirstComponent,
    ExoComponent,
    TemplatingComponent,
    StudentComponent,
    DogListComponent,
    DogComponent,
    BreedManagerComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [FirstComponent]
})
export class AppModule { }
