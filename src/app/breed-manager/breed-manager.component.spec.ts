import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreedManagerComponent } from './breed-manager.component';

describe('BreedManagerComponent', () => {
  let component: BreedManagerComponent;
  let fixture: ComponentFixture<BreedManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BreedManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreedManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
