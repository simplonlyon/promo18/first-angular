import { Component, OnInit } from '@angular/core';
import { BreedProviderService } from '../breed-provider.service';

@Component({
  selector: 'app-breed-manager',
  templateUrl: './breed-manager.component.html',
  styleUrls: ['./breed-manager.component.css']
})
export class BreedManagerComponent implements OnInit {

  list: string[] = [];
  model:string = "";
  submitted = false;

  constructor(private bp: BreedProviderService) { }

  getAll() {
    return this.list;
  }

  onSubmit():void {
    this.bp.add(this.model);
    this.list = this.bp.getBreeds();
    this.model = "";
  }



  ngOnInit(): void {
    this.list = this.bp.getBreeds();
  }


}
