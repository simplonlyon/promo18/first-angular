import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StudentProviderService {

  students: string[] = ['Serigne','Jim','Amine','Camille T','Romain','Mohammad','Camille A','Jason','Damien','Safik','Mustapha','Henrique','Aurélien','Tarik','Jean-Marc'];

  constructor() { }

  getStudents():string[] {
    return this.students;
  }

  add(student: string): void {
    this.students.push(student);
  }
}
