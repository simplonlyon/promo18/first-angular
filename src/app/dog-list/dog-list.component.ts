import { Component, OnInit } from '@angular/core';
import { DogProviderService } from '../dog-provider.service';
import { Dog } from '../entities';

@Component({
  selector: 'app-dog-list',
  templateUrl: './dog-list.component.html',
  styleUrls: ['./dog-list.component.css']
})
export class DogListComponent implements OnInit {

  list: Dog[] = [];

  constructor(private dp: DogProviderService) { }

  getAll(): void {
    this.list = this.dp.getDogs();
  }

  sortById(): void {
    this.list = this.dp.getDogsOrderedById();
  }

  sortByName(): void {
    this.list = this.dp.getDogsOrderedByName();
  }

  sortByBreed(): void {
    this.list = this.dp.getDogsOrderedByBreed();
  }

  sortByBirth(): void {
    this.list = this.dp.getDogsOrderedByBirth();
  }

  ngOnInit(): void {
    this.getAll();
  }

}
