import { Component, OnInit } from '@angular/core';
import { StudentProviderService } from '../student-provider.service';

@Component({
  selector: 'app-templating',
  templateUrl: './templating.component.html',
  styleUrls: ['./templating.component.css']
})
export class TemplatingComponent implements OnInit {
  students:string[] = [];
  modalOpen:boolean = false;

  constructor(private st: StudentProviderService) { }

  ngOnInit(): void {
    this.students = this.st.getStudents();
  }

  toggleModal() {
    this.modalOpen = !this.modalOpen;
   
  }



}


