import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BreedProviderService {
  private breeds: string[] = [
    'Shiba',
    'Corgi',
    'Crossbred',
    'Samoyed',
    'Dog'
  ];
  constructor() { }
  getBreeds(): string[] {
    return this.breeds;
  }
  add(breed:string):void {
    this.breeds.push(breed);
  }
}
