# FirstAngular

Premier projet angular.
**Attention !** Dans ce projet, dans un but pédagogique, nous avons supprimer le AppComponent pour en recréer un à la main appelé FirstComponent, dans les futurs projets il est fortement déconseillé de faire ça, les fichiers générés par défaut par le `ng new` font très bien l'affaire

## How To use
1. Cloner le projet
2. Faire un `npm install`
3. Lancer `npm start` ou `ng serve`
4. Aller sur http://localhost:4200

## Exercices

### Component compteur ([fichiers](src/app/exo))
(Les étapes 1 à 4 étaient juste là pour l'exercice, pour faire la même chose plus vite et mieux, il est préférable de faire la commande `ng g c exo` ou `ng generate component exo`, c'est la même chose)
1. Créer un dossier dans src/app qui va s'appeler exo et dedans créer un fichier exo.component.ts et exo.component.html
2. Dans le ts, faire la classe ExoComponent en se basant sur ce qu'on a déjà fait dans le FirstComponent (pas la peine de mettre de propriété pour le moment)
3. On oublie pas d'ajouter le component à la liste des declarations dans le app.module.ts
4. Dans le exo.component.html on rajoute pour le moment juste une petite balise avec un message genre "From exo" dedans
5. Dans le first.component.html on va rajouter en dessous du paragraphe une balise pour charger notre component (comme on l'a fait dans le index.html)
6. Dans le ExoComponent, rajouter une propriété counter de type number initialisée à 0 et faire en sorte de l'afficher dans le template
7. Toujours dans le ExoComponent, rajouter une méthode increment() qui va, comme son nom l'indique, incrémenter la valeur de counter de 1
8. Rajouter un button dans le template du exo.component et sur ce button, rajouter à l'intérieur des chevrons un (click)="increment()" (les différents morceaux devraient s'autocompléter) 

### Mini exo ([fichiers](src/app/exo))
1. Dans la classe ExoComponent, rajouter une méthode decrement() qui va décrémenter la valeur du counter de 1 (en vous inspirant de la méthode increment)
2. Rajouter également une méthode reset() qui va mettre la valeur de counter à zéro
3. Dans le template, rajouter un autre button, exactement comme le premier, mais qui lui va déclencher la méthode decrement
4. Rajouter ensuite un événement au double click sur le paragraphe qui déclenchera la méthode reset() 

Bonus : faire qu'on puisse incrémenter et décrémenter avec les flêches haut et bas du clavier, et que le space remette à zéro

### Boucles ([fichiers](src/app/templating))
1. Créer un nouveau component TemplatingComponent (utiliser le ng g c pour générer le component et ses fichiers)
2. Charger ce component dans le template du FirstComponent, en dessous du app-exo par exemple
3. Dans la classe de ce component, rajouter une propriété qui va contenir un tableau de prénoms (genre les gens de la promo)
4. Dans le html du component, faire une boucle ngFor sur la liste de prénoms pour afficher tout le monde
5. Dans la classe, rajouter une méthode sayHello(name:string) qui va attendre un prénom en argument et qui va faire un alert de hello concaténé avec le prénom
6. Dans le template, rajouter un button dans le ngFor et faire en sorte qu'au click, ça déclenche la méthode sayHello en lui donnant le prénom actuel de la boucle 

### ngIf pour faire une petite modal ([fichiers](src/app/templating))
1. Dans le TemplatingComponent, rajouter une propriété modalOpen initialisée à false
2. Créer une méthode toggleModal qui passe la valeur de cette propriété de true à false ou de false à true
3. Dans le template, rajouter un élément div avec du contenu dedans, peu importe, et faire que cette élément ne s'affiche que si modalOpen est true
4. Rajouter un button qui, au click, va déclencher la méthode toggleModal()
5. Faire un peu de css pour faire que la modal s'affiche en effet comme une modal, genre fixed au milieu de l'écran etc. 

### Système d'onglet ([fichiers](src/app))
1. Dans le FirstComponent, rajouter une propriété currentDisplay en string qui aura comme valeur par défaut 'exo'
2. Dans le template du first.component, rajouter des *ngIf sur le app-exo et sur le app-templating pour faire que l'un s'affiche si currentDisplay contient 'exo' et que l'autre s'affiche si currentDisplay contient 'templating'
3. Rajouter au dessus un bouton par component (donc là 2 pour le moment), et faire que quand on click sur un des bouton, ça mette 'exo' dans currentDisplay et quand on clique sur l'autre ça mette 'templating' dedans (il est possible de faire directement l'assignation dans le (click) sans avoir besoin de passer par une fonction)
4. Rajouter un ngClass sur les button pour faire que ça change l'aspect du button actuellement affiché (en gros il faut créer une classe css, et dans le ngClass, faire en sorte de l'assigner au button seulement si currentDisplay est égal au component lié au bouton actuel)

Bonus : Faire en sorte d'afficher les boutons via une boucle et un tableau pour que le truc soit plus évolutif et rapide à écrire

### StudentComponent avec Input  ([ici](src/app/student/) et [là](src/app/exo/exo.component.html))
1. Générer un nouveau component StudentComponent qui aura en propriété, pour l'instant, un name
2. Faire en sorte que ce name soit un Input et faire dans le template qu'on affiche un button avec le name dedans, exactement comme on avait fait dans le ngFor du templating.component.html
3. Dans le templating.component.html, retirer le button du ngFor, et à la place, mettre la balise du component student qu'on vient de créer, en lui donnant en paramètre la valeur actuelle de la boucle
4. Couper/coller la méthode sayHello du TemplatingComponent vers le StudentComponent, et l'adapter pour faire qu'elle n'attende plus de name en argument, mais qu'elle utilise directement la propriété name du StudentComponent
5. Assigner le sayHello en event au click sur le button, comme avant, mais cette fois ci, pas besoin de lui passer d'argument 


### La liste de chien ([dog list](src/app/dog-list/) et [dog](src/app/dog))
1. Créer un type ou une interface Dog dans un fichier entities.ts que vous mettrez dans le dossier src/app. Elle aura donc un id en number, un name en string, une breed en string et birthdate en Date
2. Générer un component DogListComponent et dans celui ci, mettre une propriété list qui contiendra un array de Dog. Initialiser cet array avec quelques chiens
3. Créer un component DogComponent, et dans celui ci, mettre une propriété dog de type Dog qui sera également un input
4. Faire le template du DogComponent en faisant en sorte d'afficher les différentes informations du chien, pour le moment, de manière un peu moche, les unes sous les autres
5. Dans le template du DogList, faire un ngFor sur la liste de de chien pour faire en sorte d'afficher un DogComponent par chien, en lui donnant en attribut le dog, comme on l'a fait dans l'exercice d'avant avec les Student
6. Prendre le link de bootstrap et le charger dans le src/index.html, puis changer la structure HTML du template du DogComponent pour le représenter son forme de card bootstrap
7. Modifier le template du DogList pour faire en sorte que chaque app-dog soit à l'intérieur d'une col, elle même  à l'intérieur d'une row 

